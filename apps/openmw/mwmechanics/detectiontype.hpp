#ifndef OPENMW_MWMECHANICS_DETECTIONTYPE_H
#define OPENMW_MWMECHANICS_DETECTIONTYPE_H

namespace MWMechanics
{
    enum DetectionType
    {
        Detect_Enchantment,
        Detect_Key,
        Detect_Creature
    };
}

#endif
